from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import Form, ModelForm
import home.models as models


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class LoginForm(Form):
    username = forms.CharField(max_length=128)
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        fields = ['username', 'password']


class MovieForm(ModelForm):
    provider_token = forms.CharField(max_length=12)
    length = forms.DurationField()
    trailer_id = forms.CharField(max_length=12, required=False)
    image_id = forms.CharField(max_length=12, required=False)

    class Meta:
        model = models.Content
        fields = ['rate', 'name', 'description', 'release_date', 'provider_token', 'length', 'trailer_id', 'image_id']


class SeriesForm(ModelForm):
    provider_token = forms.CharField(max_length=12)
    trailer_id = forms.CharField(max_length=12, required=False)
    image_id = forms.CharField(max_length=12, required=False)

    class Meta:
        model = models.Content
        fields = ['rate', 'name', 'description', 'release_date', 'provider_token', 'trailer_id', 'image_id']


class SeasonForm(Form):
    index = forms.IntegerField()
    release_date = forms.DateTimeField()
    trailer_id = forms.CharField(max_length=12, required=False)
    image_id = forms.CharField(max_length=12, required=False)
    series_id = forms.CharField(max_length=12)


class EpisodeForm(Form):
    index = forms.IntegerField()
    release_date = forms.DateTimeField()
    name = forms.CharField(max_length=128)
    trailer_id = forms.CharField(max_length=12, required=False)
    image_id = forms.CharField(max_length=12, required=False)
    season_id = forms.CharField(max_length=12)


class FilterForm(Form):
    start_date = forms.DateTimeField(required=False)
    end_date = forms.DateTimeField(required=False)
    name = forms.CharField(max_length=128, required=False)
    actor = forms.CharField(max_length=128, required=False)
    rate = forms.FloatField(required=False)


