# Generated by Django 3.0.2 on 2020-01-22 18:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_provider_identifier'),
    ]

    operations = [
        migrations.AlterField(
            model_name='content',
            name='rate',
            field=models.FloatField(),
        ),
    ]
