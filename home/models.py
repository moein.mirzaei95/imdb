from django.db import models
from django.utils.crypto import get_random_string


class Provider(models.Model):
    identifier = models.CharField(default=get_random_string, max_length=12, unique=True)
    name = models.CharField(max_length=128)
    email = models.CharField(max_length=128)

    @classmethod
    def get_provider(cls, provider_token):
        return cls.objects.filter(identifier=provider_token).first()


class Content(models.Model):
    rate = models.FloatField()
    name = models.CharField(max_length=128)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    description = models.TextField()
    release_date = models.DateTimeField()
    provider = models.ForeignKey(Provider, on_delete=models.SET_NULL, null=True)


class Genre(models.Model):
    name = models.CharField(max_length=128)


class ContentGenre(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)


class File(models.Model):
    extension = models.CharField(max_length=8)
    name = models.CharField(max_length=256)
    url = models.CharField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)
    size = models.IntegerField()


class Trailer(models.Model):
    identifier = models.CharField(default=get_random_string, max_length=12, unique=True)
    length = models.DurationField()
    resolution = models.CharField(max_length=6)
    file = models.ForeignKey(File, on_delete=models.CASCADE)

    @classmethod
    def get_trailer(cls, identifier):
        return cls.objects.filter(identifier=identifier).first()


class Image(models.Model):
    identifier = models.CharField(default=get_random_string, max_length=12, unique=True)
    width = models.CharField(max_length=8)
    height = models.CharField(max_length=8)
    file = models.ForeignKey(File, on_delete=models.CASCADE)

    @classmethod
    def get_image(cls, identifier):
        return cls.objects.filter(identifier=identifier).first()


class Movie(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    length = models.DurationField()
    trailer = models.ForeignKey(Trailer, on_delete=models.SET_NULL, null=True)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)


class Series(models.Model):
    identifier = models.CharField(default=get_random_string, max_length=12, unique=True)
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    trailer = models.ForeignKey(Trailer, on_delete=models.SET_NULL, null=True)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)

    @classmethod
    def get_series(cls, id):
        return cls.objects.filter(identifier=id).first()


class Season(models.Model):
    identifier = models.CharField(default=get_random_string, max_length=12, unique=True)
    index = models.IntegerField()
    release_date = models.DateTimeField()
    series = models.ForeignKey(Series, on_delete=models.CASCADE)
    trailer = models.ForeignKey(Trailer, on_delete=models.SET_NULL, null=True)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)

    @classmethod
    def get_season(cls, id):
        return cls.objects.filter(identifier=id).first()


class Episode(models.Model):
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    index = models.IntegerField()
    release_date = models.DateTimeField()
    name = models.CharField(max_length=128)
    trailer = models.ForeignKey(Trailer, on_delete=models.SET_NULL, null=True)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)


class Actor(models.Model):
    birth_date = models.DateTimeField()
    name = models.CharField(max_length=32)
    email = models.CharField(max_length=128)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)


class ContentActor(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    actor = models.ForeignKey(Actor, on_delete=models.CASCADE)
