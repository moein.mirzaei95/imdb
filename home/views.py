from django.contrib.auth import login as login_user, authenticate
from django.http import JsonResponse

from home.models import Provider, Content, Movie, Trailer, Image, Series, Season, Episode, Actor
from .forms import SignUpForm, LoginForm, MovieForm, SeriesForm, SeasonForm, EpisodeForm, FilterForm
from django.contrib.auth import logout as logout_user
from django.shortcuts import render, redirect

from django.utils.dateparse import parse_duration


def home(request):
    if request.method == 'POST':
        form = FilterForm(request.POST)
        if form.is_valid():
            queryset = Content.objects.all()
            if form.data['start_date']:
                queryset = queryset.filter(release_date__gte=form.data['start_date'])
            if form.data['end_date']:
                queryset = queryset.filter(release_date__lte=form.data['end_date'])
            if form.data['rate']:
                queryset = queryset.filter(rate__gte=form.data['rate'])
            if form.data['name']:
                queryset = queryset.filter(name__contains=form.data['name'])
            if form.data['actor']:
                actor = Actor.objects.filter(name=form.data['actor']).first()
                queryset = queryset.filter(contentactor__actor=actor)
            movies = Movie.objects.filter(content_id__in=queryset)
            series = Series.objects.filter(content_id__in=queryset)
            return render(request, 'home.html', {'form': form, 'movies': movies, 'series': series})
    else:
        form = FilterForm()
        return render(request, 'home.html', {'form': form})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login_user(request, user)
            return redirect('home:home')
    else:
        form = SignUpForm()
        return render(request, 'signup.html', {'form': form})


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=raw_password)
            if user:
                login_user(request, user)
            else:
                redirect('home:login')
        return redirect('home:home')
    else:
        form = LoginForm()
        return render(request, 'login.html', {'form':form})


def logout(request):
    if request.method == 'GET':
        logout_user(request)
        return redirect('home:home')


def get_trailer(trailer):
    if trailer:
        trailer = Trailer.get_trailer(trailer)
        if trailer:
            return trailer, False
        else:
            return None, True
    else:
        return None, False


def get_image(image):
    if image:
        image = Image.get_image(image)
        if image:
            return image, False
        else:
            return None, True
    else:
        return None, False


def create_movie(request):
    if request.method == 'POST':
        form = MovieForm(request.POST)
        if form.is_valid():
            provider = Provider.get_provider(form.data['provider_token'])
            trailer, t_status = get_trailer(form.data['trailer_id'])
            image, i_status = get_image(form.data['image_id'])
            if not provider:
                return redirect('home:create-movie')
            if t_status or i_status:
                return redirect('home:create-movie')
            content = Content.objects.create(rate=form.data['rate'], release_date=form.data['release_date'],
                                             name=form.data['name'], description=form.data['description'],
                                             provider=provider)
            Movie.objects.create(content=content, length=parse_duration(form.data['length']), trailer=trailer, image=image)
            return redirect('home:home')
    else:
        form = MovieForm()
        return render(request, 'movie-creation.html', {'form': form})


def create_series(request):
    if request.method == 'POST':
        form = SeriesForm(request.POST)
        if form.is_valid():
            provider = Provider.get_provider(form.data['provider_token'])
            trailer, t_status = get_trailer(form.data['trailer_id'])
            image, i_status = get_image(form.data['image_id'])
            if not provider:
                return redirect('home:create-series')
            if t_status or i_status:
                return redirect('home:create-series')
            content = Content.objects.create(rate=form.data['rate'], release_date=form.data['release_date'],
                                             name=form.data['name'], description=form.data['description'],
                                             provider=provider)
            Series.objects.create(content=content, trailer=trailer, image=image)
            return redirect('home:home')
    else:
        form = SeriesForm()
        return render(request, 'series-creation.html', {'form': form})


def create_season(request):
    if request.method == 'POST':
        form = SeasonForm(request.POST)
        if form.is_valid():
            series = Series.get_series(form.data['series_id'])
            trailer, t_status = get_trailer(form.data['trailer_id'])
            image, i_status = get_image(form.data['image_id'])
            if not series:
                return redirect('home:create-season')
            if t_status or i_status:
                return redirect('home:create-season')

            Season.objects.create(index=form.data['index'], series=series, release_date=form.data['release_date'],
                                  trailer=trailer, image=image)
            return redirect('home:home')
    else:
        form = SeasonForm()
        return render(request, 'season-creation.html', {'form': form})


def create_episode(request):
    if request.method == 'POST':
        form = EpisodeForm(request.POST)
        if form.is_valid():
            season = Season.get_season(form.data['season_id'])
            trailer, t_status = get_trailer(form.data['trailer_id'])
            image, i_status = get_image(form.data['image_id'])
            if not season:
                return redirect('home:create-season')
            if t_status or i_status:
                return redirect('home:create-season')
            Episode.objects.create(index=form.data['index'], release_date=form.data['release_date'],
                                   name=form.data['name'], season=season, trailer=trailer, image=image)
            return redirect('home:home')
    else:
        form = EpisodeForm()
        return render(request, 'episode-creation.html', {'form': form})


def get_provider_series(request, identifier):
    series = Series.objects.select_related('content').filter(content__provider__identifier=identifier)
    result = {}
    for item in series:
        result[item.content.name] = item.identifier
    return JsonResponse(result)


def get_provider_seasons(request, identifier):
    seasons = Season.objects.select_related('series__content').filter(series__content__provider__identifier=identifier)
    result = {}
    for item in seasons:
        result[str((item.series.content.name, item.index))] = item.identifier
    return JsonResponse(result)
