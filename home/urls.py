from django.urls import path

import home.views as views

app_name = "home"
urlpatterns = [
    path('', views.home, name='home'),
    path("signup/", views.signup, name='signup'),
    path("logout/", views.logout, name='logout'),
    path("login/", views.login, name='login'),
    path("create-movie/", views.create_movie, name='create-movie'),
    path("create-series/", views.create_series, name='create-series'),
    path("create-season/", views.create_season, name='create-season'),
    path("create-episode/", views.create_episode, name='create-episode'),
    path("provider-series/<str:identifier>", views.get_provider_series, name='provider-series'),
    path("provider-season/<str:identifier>", views.get_provider_seasons, name='provider-season'),
]
